require 'socket'

system("cls") or system("clear")

TIMEOUT = 10

def scan(host,port)
	socket 			= Socket.new(:INET,:STREAM)
	rem 			= Socket.sockaddr_in(port,host)
	begin 
		socket.connect_nonblock(rem)
	rescue Errno::EINPROGRESS
	end
	_, sockets, _ = IO.select(nil,[socket],nil,TIMEOUT)
	if sockets
		puts "\e[1;32m[\e[1;34m*\e[1;32m] \e[1;37mport \e[1;36m#{port} \e[1;37m--> \e[1;33mopen"
	else
		puts "\e[1;31m[\e[1;30m+\e[1;31m] \e[1;30mport \e[1;33m#{port} \e[1;34mis closed"
	end
end

print "\e[1;34m[\e[1;33m?\e[1;34m] \e[1;37mhost to scan [\e[1;36m127.0.0.1/www.example.com\e[1;37m]   \e[1;34m"
host = gets.chomp()
threads = []
port_lis = [9,20,21,22,23,25,37,42,43,49,67,68,69,79,80,88,109,110,111,218,209,427,491,513,561,660,694,752,754,989,990,1241,1311,3306,4001,5000,5050,5985,1443,443,53,1443,135]

port_lis.each { |i|
	threads << Thread.new {
		scan(host,i)
	}
}
threads.each(&:join)

puts "\e[1;37mScan successfull !"